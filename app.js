// Import Express && Body Parser
const express = require('express');
const bodyParser = require('body-parser');
const graphQlHttp = require('express-graphql');
const { buildSchema } = require('graphql');

// Create express object
const app = express();

const events = [];

// Use body parser middleware to use incoming json requests
app.use(bodyParser.json());

app.get('/', (req, res, next) => {
    res.send('Express Starts');
});

app.use('/graphQlApi', graphQlHttp({
    schema: buildSchema(`
        type Event {
            _id: ID!
            title: String!
            description: String
            date: String!
            venue: String
        }

        input EventInput {
            title: String!
            description: String
            date: String!
            venue: String
        }

        type RootQuery {
            events: [Event!]!
        }

        type RootMutation {
            createEvent(eventInput: EventInput): Event
        }

        schema {
            query: RootQuery
            mutation: RootMutation
        }
    `),
    rootValue: {
        events: () => {
            return events;
        },
        createEvent: (args) => {
            const event = {
                _id: Math.random().toString(),
                title: args.eventInput.title,
                description: args.eventInput.description,
                date: args.eventInput.date,
                venue: args.eventInput.venue
            }
            return event;
            events.push(event);

        }
    },
    graphiql: true
}));

// Start server on port 3000
app.listen(3002);